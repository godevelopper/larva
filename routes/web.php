<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('shares', 'ShareController');

Route::resource('/products', 'ProductController');

Route::resource('/customers', 'CustomerController');

Route::resource('/order', 'OrderController');

Route::resource('/adminlte', 'AdminLteController');

Route::resource('ajax-crud', 'AjaxController');


Route::get('manage-item-ajax', 'ArtikelController@manageItemAjax');
Route::resource('item-ajax', 'ArtikelController');

// new routes

Route::resource('/links', 'LinkController');




//--CREATE a link--//
// Route::post('/links', function (Request $request) {
//     $link = Link::create($request->all());
//     return Response::json($link);
// });

// //--GET LINK TO EDIT--//
// Route::get('/links/{link_id?}', function ($link_id) {
//     $link = Link::find($link_id);
//     return Response::json($link);
// });

// //--UPDATE a link--//
// Route::put('/links/{link_id?}', function (Request $request, $link_id) {
//     $link = Link::find($link_id);
//     $link->url = $request->url;
//     $link->description = $request->description;
//     $link->save();
//     return Response::json($link);
// });

//--DELETE a link--//
// Route::delete('/links/{link_id?}', function ($link_id) {
//     $link = Link::destroy($link_id);
//     return Response::json($link);
// });
