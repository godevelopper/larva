<?php

use Illuminate\Database\Seeder;
use App\Share;

class ShareDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $share = Share::create([
          'share_name'      =>  'artikel',
          'share_price'     => 5000,
          'share_qty'       => 5
        ]);
    }
}
