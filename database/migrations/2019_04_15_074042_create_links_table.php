<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Link;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url');
            $table->string('description');
            $table->timestamps();
        });

        Link::create([
          'url'             =>  'www.google.com',
          'description'     =>  'ini adalah google'
        ]);

        Link::create([
            'url'             =>  'www.facebook.com',
            'description'     =>  'ini adalah facebook'
          ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
