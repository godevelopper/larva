
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel AJAX CRUD</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.5/css/bootstrap-flex.min.css">

    <!-- Custom styles for this template -->
   <link href="{{ asset('css/sticky-footer-navbar.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">

        <div class="card card-block">
            <h2 class="card-title">Laravel AJAX Examples
                <small>via jQuery .ajax()</small>
            </h2>
            <p class="card-text">Learn how to handle ajax with Laravel and jQuery.</p>
            <button id="btn-add" name="btn-add" class="btn btn-primary btn-xs">Add New Link</button>
        </div>

        <div>
            <table class="table table-inverse">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Link</th>
                    <th>Description</th>
                    <th>Edit or Delete</th>
                </tr>
                </thead>
                <tbody id="links-list" name="links-list">
                @foreach ($links as $link)
                    <tr id="link{{$link->id}}">
                        <td>{{$link->id}}</td>
                        <td>{{$link->url}}</td>
                        <td>{{$link->description}}</td>
                        <td>
                            <button class="btn btn-info open-modal" value="{{$link->id}}">Edit
                            </button>
                            <button class="btn btn-danger delete-link" value="{{$link->id}}">Delete
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="modal fade" id="linkEditorModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="linkEditorModalLabel">Link Editor</h4>
                        </div>
                        <div class="modal-body">
                            <form id="modalFormData" name="modalFormData" class="form-horizontal" novalidate="">

                                <div class="form-group">
                                    <label for="inputLink" class="col-sm-2 control-label">Link</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="link" name="link"
                                               placeholder="Enter URL" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="description" name="description"
                                               placeholder="Enter Link Description" value="">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes
                            </button>
                            <input type="hidden" id="link_id" name="link_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.4/js/tether.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>


    <script>
      $(document).ready(function () {
    ////----- Open the modal to CREATE a link -----////
        $('#btn-add').click(function () {
            console.log("tombol btn-add diklik");

            $('#btn-save').val("add");
            $('#modalFormData').trigger("reset");
            $('#linkEditorModal').modal('show');
        });

        ////----- Open the modal to UPDATE a link -----////
        $('body').on('click', '.open-modal', function () {
            var link_id = $(this).val();
            $.get('links/' + link_id, function (data) {
                $('#link_id').val(data.id);
                $('#link').val(data.url);
                $('#description').val(data.description);
                $('#btn-save').val("update");
                $('#linkEditorModal').modal('show');
            })
        });

        // Clicking the save button on the open modal for both CREATE and UPDATE
        $("#btn-save").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();
            var formData = {
                url: $('#link').val(),
                description: $('#description').val(),
            };
            var state = $('#btn-save').val();
            var type = "POST";
            var link_id = $('#link_id').val();
            var ajaxurl = 'links';
            if (state == "update") {
                type = "PUT";
                ajaxurl = 'links/' + link_id;
            }
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    var link = '<tr id="link' + data.id + '"><td>' + data.id + '</td><td>' + data.url + '</td><td>' + data.description + '</td>';
                    link += '<td><button class="btn btn-info open-modal" value="' + data.id + '">Edit</button>&nbsp;';
                    link += '<button class="btn btn-danger delete-link" value="' + data.id + '">Delete</button></td></tr>';
                    if (state == "add") {
                        $('#links-list').append(link);
                    } else {
                        $("#link" + link_id).replaceWith(link);
                    }
                    $('#modalFormData').trigger("reset");
                    $('#linkEditorModal').modal('hide')
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        ////----- DELETE a link and remove from the page -----////
        $('.delete-link').click(function () {
            var link_id = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "DELETE",
                url: 'links/' + link_id,
                success: function (data) {
                    console.log(data);
                    $("#link" + link_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    });

    </script>





</body>
</html>



