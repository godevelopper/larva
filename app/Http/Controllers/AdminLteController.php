<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class AdminLteController extends Controller
{
    public function index()
    {
        $item = Item::all();

        return view('admin.index' , compact('item' , $item) );
    }
}
