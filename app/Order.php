<?php

namespace App;

use App\Item;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $table = 'orders';
  protected $fillable = ['user_id', 'item_id'];

  public function items()
  {
     return $this->belongsTo(Item::class);
  }
}
